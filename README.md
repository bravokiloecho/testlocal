# A tiny plugin for the Craft CMS

## Use to test whether the site is running locally.

### Usage:

Put this folder in the Craft plugins folder. Then enable in the Plugins section in the CMS.

In your templates, use it like this:

```
{% if craft.testLocal.isLocal() %}
	input local specific things
{% else %}
	input live specific things
{% endif %}
```