<?php
namespace Craft;

class testLocalPlugin extends BasePlugin
{
	/*	CONFIG
	============================================================== */

	function getName()
	{
		return Craft::t('testLocal');
	}

	function getVersion()
	{
		return '1.0';
	}

	function getDeveloper()
	{
		return 'dk8';
	}

	function getDeveloperUrl()
	{
		return 'http://dk8.co';
	}

	public function hasCpSection()
	{
		return false;
	}

}